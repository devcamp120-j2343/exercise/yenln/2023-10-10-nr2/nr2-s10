const getAllUsersMiddlewares = (req, res, next) => {
    console.log("Get all Users Middleware");

    next();
}

const createUsersMiddlewares = (req, res, next) => {
    console.log("Create Users Middleware");

    next();
}

const getDetailUsersMiddlewares = (req, res, next) => {
    console.log("Get detail User Middleware");

    next();
}

const updateUsersMiddlewares = (req, res, next) => {
    console.log("Update Users Middleware");

    next();
}

const deleteUsersMiddlewares = (req, res, next) => {
    console.log("Delete Users Middleware");

    next();
}

module.exports = {
    getAllUsersMiddlewares,
    createUsersMiddlewares, 
    getDetailUsersMiddlewares,
    updateUsersMiddlewares,
    deleteUsersMiddlewares
}