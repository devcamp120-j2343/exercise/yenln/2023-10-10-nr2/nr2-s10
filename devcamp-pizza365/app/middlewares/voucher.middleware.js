const getAllVouchersMiddlewares = (req, res, next) => {
    console.log("Get all Vouchers Middleware");

    next();
}

const createVouchersMiddlewares = (req, res, next) => {
    console.log("Create Vouchers Middleware");

    next();
}

const getDetailVouchersMiddlewares = (req, res, next) => {
    console.log("Get detail Voucher Middleware");

    next();
}

const updateVouchersMiddlewares = (req, res, next) => {
    console.log("Update Vouchers Middleware");

    next();
}

const deleteVouchersMiddlewares = (req, res, next) => {
    console.log("Delete Vouchers Middleware");

    next();
}

module.exports = {
    getAllVouchersMiddlewares,
    createVouchersMiddlewares, 
    getDetailVouchersMiddlewares,
    updateVouchersMiddlewares,
    deleteVouchersMiddlewares
}