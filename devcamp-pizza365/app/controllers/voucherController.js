const mongoose = require("mongoose");

const voucherModel = require("../model/voucherModel");
const voucherRouter = require("../routes/voucherRouter");

const createVoucher = async (req, res) => {
    //Thao tác với CSDL
    const {
        maVoucher,
        phanTramGiamGia,
        ghiChu
    } = req.body;

    if (!phanTramGiamGia || phanTramGiamGia < 0) {
        return res.status(400).json({
            message: "phanTramGiamGia khong hop le",
        });
    }

    const newVoucher = {
        _id:  new mongoose.Types.ObjectId(),
        maVoucher,
        phanTramGiamGia,
        ghiChu
    }

    voucherModel.create(newVoucher)
        .then((data) => {
            return res.status(201).json({
                status: "Create new voucher sucessfully",
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

const getAllVoucher = (req, res) => {
    voucherModel.find()
        .then((data) => {
            if (data && data.length > 0) {
                return res.status(201).json({
                    status: "Get all vouchers sucessfully",
                    data
                })
            }
            else {
                return res.status(404).json({
                    status: "Not found any voucher",
                    data
                })

            }
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

const getVoucherById = async (req, res) => {
    var voucherId = req.params.voucherId;

    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "voucherId is invalid!"
        })
    }

    voucherModel.findById(voucherId)
        .then((data) => {
            if (!data) {
                return res.status(404).json({
                    status: "Not found detail of this voucher",
                    data
                })
            }
            else {
                return res.status(201).json({
                    status: `Get voucher ${voucherId} sucessfully`,
                    data
                })

            }
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}

const updateVoucherById = async (req, res) => {
    //B1: thu thập dữ liệu
    const voucherId = req.params.voucherId;

    const { maVoucher, phanTramGiamGia, ghiChu } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "voucherId is invalid!"
        })
    }

    if (phanTramGiamGia < 0) {
        return res.status(400).json({
            status: "Bad request",
            message: "phanTramGiamGia is larger than 0!"
        })
    }
    if (maVoucher == "") {
        return res.status(400).json({
            status: "Bad request",
            message: "maVoucher is invalid!"
        })
    }

    //B3: thực thi model
    try {
        let updatedVoucher = {

        }

        if (maVoucher) {
            updatedVoucher.maVoucher = maVoucher;
        }
        if (phanTramGiamGia) {
            updatedVoucher.phanTramGiamGia = phanTramGiamGia;
        }
        if (ghiChu) {
            updatedVoucher.ghiChu = ghiChu;
        }

        const result = await voucherModel.findByIdAndUpdate(
            _id,
            updatedVoucher
        );

        if (result) {
            return res.status(200).json({
                status: "Update voucher sucessfully",
                data: updatedVoucher
            })
        } else {
            return res.status(404).json({
                status: "Not found any voucher"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const deleteVoucherById = async (req, res) => {
    //B1: Thu thập dữ liệu
    var voucherId = req.params.voucherId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }

    try {
        const deletedVoucher = await voucherModel.findByIdAndDelete(voucherId);

        if (deletedVoucher) {
            return res.status(200).json({
                status: `Delete voucher ${voucherId} sucessfully`,
                data: deletedVoucher
            })
        } else {
            return res.status(404).json({
                status: "Not found any voucher"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

module.exports = {
    createVoucher,
    getAllVoucher,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
}
