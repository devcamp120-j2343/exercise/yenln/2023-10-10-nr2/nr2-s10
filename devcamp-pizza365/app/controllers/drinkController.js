const mongoose = require("mongoose");

const drinkModel = require("../model/drinkModel")
const drinkRouter = require("../routes/drinkRouter");

const createDrink = async (req, res) => {
    //Thao tác với CSDL
    const {
        maNuocUong,
        tenNuocUong,
        donGia
    } = req.body;

    if (!maNuocUong ) {
        return res.status(400).json({
            message: "maNuocUong khong hop le",
        });
    }
    if (!tenNuocUong ) {
        return res.status(400).json({
            message: "tenNuocUong khong hop le",
        });
    }
    if (!donGia || isNaN(donGia)|| donGia < 0) {
        return res.status(400).json({
            message: "donGia khong hop le",
        });
    }


    const newdrink = {
        _id:  new mongoose.Types.ObjectId(),
        maNuocUong,
        tenNuocUong,
        donGia
    }

    drinkModel.create(newdrink)
        .then((data) => {
            return res.status(201).json({
                status: "Create new drink sucessfully",
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

const getAllDrink = (req, res) => {
    drinkModel.find()
        .then((data) => {
            if (data && data.length > 0) {
                return res.status(201).json({
                    status: "Get all drinks sucessfully",
                    data
                })
            }
            else {
                return res.status(404).json({
                    status: "Not found any drink",
                    data
                })

            }
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

const getDrinkById = async (req, res) => {
    var drinkId = req.params.drinkId;

    if (!mongoose.Types.ObjectId.isValid(drinkId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "drinkId is invalid!"
        })
    }

    drinkModel.findById(drinkId)
        .then((data) => {
            if (!data) {
                return res.status(404).json({
                    status: "Not found detail of this drink",
                    data
                })
            }
            else {
                return res.status(201).json({
                    status: `Get drink ${drinkId} sucessfully`,
                    data
                })

            }
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}

const updateDrinkById = async (req, res) => {
    //B1: thu thập dữ liệu
    const drinkId = req.params.drinkId;

    const { maNuocUong, tenNuocUong, donGia } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(drinkId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "drinkId is invalid!"
        })
    }

    if (!donGia  || !isNaN(donGia)|| donGia < 0) {
        return res.status(400).json({
            message: "donGia khong hop le",
        });
    }
    if (maNuocUong == "") {
        return res.status(400).json({
            status: "Bad request",
            message: "maNuocUong is invalid!"
        })
    }
    if (tenNuocUong == "") {
        return res.status(400).json({
            status: "Bad request",
            message: "tenNuocUong is invalid!"
        })
    }


    //B3: thực thi model
    try {
        let updatedDrink = {

        }

        if (maNuocUong) {
            updatedDrink.maNuocUong = maNuocUong;
        }
        if (tenNuocUong) {
            updatedDrink.tenNuocUong = tenNuocUong;
        }
        if (donGia) {
            updatedDrink.donGia = donGia;
        }

        const result = await drinkModel.findByIdAndUpdate(
            _id,
            updatedDrink
        );

        if (result) {
            return res.status(200).json({
                status: "Update drink sucessfully",
                data: updatedDrink
            })
        } else {
            return res.status(404).json({
                status: "Not found any drink"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const deleteDrinkById = async (req, res) => {
    //B1: Thu thập dữ liệu
    var drinkId = req.params.drinkId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(drinkId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }

    try {
        const deletedDrink = await drinkModel.findByIdAndDelete(drinkId);

        if (deletedDrink) {
            return res.status(200).json({
                status: `Delete drink ${drinkId} sucessfully`,
                data: deletedDrink
            })
        } else {
            return res.status(404).json({
                status: "Not found any drink"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

module.exports = {
    createDrink,
    getAllDrink,
    getDrinkById,
    updateDrinkById,
    deleteDrinkById
}
