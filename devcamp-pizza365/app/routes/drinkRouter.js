//import { Express } from "express";
const express = require("express");

const drinkMiddleware = require("../middlewares/drink.middleware");

const drinkController = require("../controllers/drinkController")

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
});
 
router.get("/", drinkController.getAllDrink)

router.post("/", drinkController.createDrink)

router.get("/:drinkId", drinkController.deleteDrinkById)

router.put("/:drinkId", drinkController.updateDrinkById)

router.delete("/:drinkId", drinkController.deleteDrinkById)

module.exports = router;