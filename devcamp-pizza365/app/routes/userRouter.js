//import { Express } from "express";
const express = require("express");

const userMiddleware = require("../middlewares/user.middleware");
const userController = require("../controllers/userController");

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
});
 
router.get("/limit-users", userController.getAllUser);

router.get("/skip-users", userController.getAllUser);

router.get("/sort-users", userController.getAllUserWithSort);

router.get("/skip-limit-users", userController.getAllUserWithSkip);

router.get("/sort-skip-limit-users", userController.getAllUserWithLimit);

router.get("/", userController.getAllUser);

router.post("/", userController.createUser); 

router.get("/:userId", userController.getUserById);

router.put("/:userId", userController.updateUserById);

router.delete("/:userId", userController.deleteUserById)

module.exports = router;